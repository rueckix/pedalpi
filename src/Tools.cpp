#include "Tools.h"
#include <limits>

namespace Tools
{

template <typename T>
T Sign(const T &a) {
    return ((a > 0) - (a < 0));
}
// template declaration
template int Sign<int>(const int &a);
template float Sign<float>(const float &a);

template <typename T>
T Compare(const T &a, const T &b) {
    if (a > b + std::numeric_limits<T>::epsilon())
        return 1;
    else if (a < b - std::numeric_limits<T>::epsilon())
        return -1;
    else
        return 0;
        
}
// template declaration
template int Compare<int>(const int &a, const int &b);
template float Compare<float>(const float &a, const float &b);


template <typename T> 
bool CanStepUp(T current,  T step,  T limit) {
    return (Compare(current, limit - step) < 1);
}
// template declaration
template bool CanStepUp<int>(int current,  int step,  int limit);
template bool CanStepUp<float>(float current,  float step,  float limit);



template <typename T>
bool CanStepDown(T current,  T step, T limit) {
    return (Compare(current, limit + step) > -1);
}

// template declaration
template bool CanStepDown<int>(int current,  int step,  int limit);
template bool CanStepDown<float>(float current,  float step,  float limit);



}