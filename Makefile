TARGET = pedalpi

CXX = g++
LINKER= gcc
# using -isystem here to switch off compiler warnings
FOREIGN_HEADERS=-isystem submodules/Q/q_lib/include/ -isystem submodules/infra/include
CXXFLAGS = -Wall -Werror -Wextra -pedantic -std=c++17  -O3  $(FOREIGN_HEADERS)



# include bcm2835 only on raspberry
ifneq (,$(findstring arm,$(shell uname -m)))
	LDBCM = -lbcm2835	
else
    LDBCM = 
endif

LDFLAGS = -lpthread

SRCDIR   = src
OBJDIR   = obj
BINDIR   = bin

SOURCES  := $(wildcard $(SRCDIR)/*.cpp)
INCLUDES := $(wildcard $(SRCDIR)/*.h)
OBJECTS  := $(SOURCES:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)
RM       = rm -f
MD		= mkdir -p

# temporary file that include all effect header #include directives
AEH = $(SRCDIR)/alleffectheaders.h
# temporary files that include code to add effects to the Pedal in Pedal.cpp
AE = $(SRCDIR)/alleffects.h
EFFECT_PREFIX = fx
ALL_EFFECT_HEADERS_DIR = $(wildcard $(SRCDIR)/$(EFFECT_PREFIX)*.h)
# remove directory
ALL_EFFECT_HEADERS = $(ALL_EFFECT_HEADERS_DIR:$(SRCDIR)/%.h=%.h)

# Get class names
EFFECT_CLASSES = $(ALL_EFFECT_HEADERS:fx%.h=%)


all: headers $(BINDIR)/$(TARGET)



# generate code snippets for Pedal.cpp
headers:
	@echo "#pragma once" > $(AEH)
	@for H in $(ALL_EFFECT_HEADERS); do \
		echo "#include \"$$H\"" >> $(AEH); \
	done

	@echo "" > $(AE)
	@for C in $(EFFECT_CLASSES); do \
		echo "assert(_effects.Add(new $$C()));" >> $(AE); \
	done
#	@touch src/Pedal.cpp


$(BINDIR)/$(TARGET): $(OBJECTS)
	$(MD) $(BINDIR)
	$(CXX) $(OBJECTS) $(LDFLAGS) $(LDBCM) -o $(BINDIR)/$(TARGET) 

$(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	$(MD) $(OBJDIR)
	$(CXX) $(DEF) $(CXXFLAGS) -c $< -o $@

.PHONY: clean
clean:
	$(RM) $(OBJECTS)
	@echo "Cleanup complete!"
	$(RM) $(BINDIR)/$(TARGET)
	@echo "Executable removed!"
	$(RM) $(AEH) $(AE)
	@echo "Temporary headers removed"
	
