#include "Interface.h"

#include <unistd.h> 
#include <fcntl.h> 
#include <termios.h> 
#include <errno.h>

class SerialInterface: public Interface
{
private:
    int _handle = -1;

public:
    SerialInterface(string port);
    ~SerialInterface();

    virtual string Read();

    virtual void Write(string line);

private:
    bool isValid() {return (_handle != -1);};
};


