#pragma once

#include <string>
#include <thread>
#include <future>
#include <atomic>
#include <iostream>

#include "debug.h"
#include "ADDA.h"
#include "Tools.h"

using namespace std;

struct effectHelp
{
    string left;
    string right;
    string center;
};


struct effectControl
{
    bool left;
    bool right;
    bool toggle;
    bool foot;
};


/**
 * Important: 
 * 1) All effects must override the pure virtual functions getHelpLeft, getHelpRight, getHelpCenter, getState, process
 * 2) Use short names and help text (the pedal-pi companion has a small display)
 * 3) Effects must be defined using header files named fx<effectname>.h for the Makefile to capture them
 * 4) All variables that are affected by UpdateState must be made thread-safe, e.g., via wrapping in atomics
 *
*/

class Effect
{
private:
    string _name;
    string _id;
    thread _thread;
    atomic<uint32_t> _samplingRate = 0;
    atomic <bool> _stopSignal = false; // using an atomic here. Using modern future and promise objects is too slow
    uint32_t _inputSignal;
    uint32_t _outputSignal;
    int32_t _sleep = 250; // used in rate limiter to achieve the desired sampling rate TARGET_SAMPLING_RATE

protected:
    float _signal;
    const int SIGNAL_MAX = 4095; // 12 bit ADC output [0, 4095]
    const int SIGNAL_MIN = 0;
    const int SIGNAL_SHIFT = 2047;
    const int CENTERED_SIGNAL_MAX = 2048;
    const int CENTERED_SIGNAL_MIN = -SIGNAL_SHIFT;

    static const uint32_t TARGET_SAMPLING_RATE = 110000; // 110 kHz is the desired sampling rate

public:
    Effect(string name, string id);
    virtual ~Effect();
    
    /**
     * 
     * @return {string}  : Display name of the effect
     */
    string GetName() const {return _name;}
    
    /**
     * 
     * @return {string}  : Unique ID of the effect
     */
    string GetID() const {return _id; };
    
    string GetState() const {return string("$:") + getState();};
         
    void Run();

    void Stop();
    /**
     * Informs the effect of hardware events. !! All affected variables must be made thread-safe
     * @param  {effectControl} control : Status of hardware switches and buttons
     */
    virtual void UpdateState(const effectControl& control );

    effectHelp GetHelp() const;

    int GetSamplingRate() const;

protected:
    void render();

    
    /**
     * Actual processing function that transforms _signal from range [-1, 1] -> [-1, 1] using floating point operations
     */
    virtual void process() = 0;
    
    /**
     * 
     * @return {string}  : Help text for left push button
     */
    virtual string getHelpLeft() const = 0;
    
    /**
     * 
     * @return {string}  : Help text for right push button
     */
    virtual string getHelpRight() const = 0;
    
    /**
     * 
     * @return {string}  : Help text for toggle switch
     */
    virtual string getHelpCenter() const = 0;
    
    
    /**
     * 
     * @return {string}          : Main effect parameter (as string)
     */
    virtual string getState() const = 0;

private:
    void mapIn();
    void mapOut();
    void readSample();
    void writeSample();

};
