#include "Pedal.h"
#include "FIFOInterface.h"
#include <assert.h>

int main(int argc, char const *argv[])
{
    //SerialInterface s("/dev/ttyAMA0");  

    assert(argc == 2);
    
    // SOCAT for testing
    // socat -d -d pty,raw,echo=0 pty,raw,echo=0
    // cat into and from the other end
    FIFOInterface s(argv[1]);  

    

    Pedal p(s);
    p.EventLoop();
    return 0;
}
