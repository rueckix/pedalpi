#include "Effect.h"

class Clean: public Effect
{
public:
    Clean();
    virtual ~Clean() {  }

protected:
    virtual void process();
    virtual string getHelpLeft() const;
    virtual string getHelpRight() const;
    virtual string getHelpCenter() const;
    virtual string getState() const;
};