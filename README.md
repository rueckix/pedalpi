# PedalPi

Refactored software for the software-defined guitar effect pedal Pedal-Pi

# Versioning
* master has the latest stable code (tagged with releases)
* dev is unstable

# Serial Communication Protocol 
As a general rule, commands are prefixed with latin characters, like "S:".
Reports are prefixed with special characters, such as "%:".

## Commands (input)
The only supported command today is the SWITCH command, prefixed with "S:", followed by the ID of an effect.
Example: "S:clean" will launch the Clean effect.

The effect must exist, otherwise and assertion is fired.

## Reports

1. List of effects
A series of "#:id:name" lines will be emitted during startup. This tells the receiver which effects are available.

2. Sampling rate
Periodically, the sampling rate is reported via "%:n kHz"

3. Selected effect
After switching an effect, the current effect will be reported via "!:id"

4. Effect Help
Upon selecting an effect, 3 hints will be displayed, indicating what the three controls (push button left, push button right, center toggle) can do.
They are reported via
* "?:L:hint"
* "?:R:hint"
* "?:C:hint"

5. Effect Parameter
Periodically the main effect parameter will be reported via "$:param". Per default, "n/a" is reported unless the effect class overrides "::getState()".

