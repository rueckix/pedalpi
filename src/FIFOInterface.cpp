#include "FIFOInterface.h"

#include <string.h>
#include "debug.h"
#include <assert.h>
#include <thread>
#include <chrono>
#include <iostream>

#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>

using namespace std;


FIFOInterface::FIFOInterface(string port) {
    struct sockaddr_un addr;
    
    _handle = socket(AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, 0);
    DBG(if (_handle == -1) cout << "FIFOInterface - Socket error" << endl;)
    
    
    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, port.c_str(), port.length());
    

    int res = connect(_handle, (struct sockaddr*)&addr, sizeof(addr));
    res = res; // hack to avoid -Werror=unused-variable compiler error when DBG is switched off
    DBG(if (res == -1) cout << "FIFOInterface - Socket connection error: " <<  port << endl;)   
}

FIFOInterface::~FIFOInterface() {
    if (isValid()) close(_handle);
}
/**
 * FIFOInterface 
 * 
 * @return {string}  : String (max 80 characters) read from serial interface. Strings are read until '\n' or EOF
 */
string FIFOInterface::Read() {
    if (!isValid()) return string("");
    
    const int size = 80;
    char buf[size];
    int idx = 0;
    for (int i = 0; i < size; i++)
    {
        char c;
        int count = read(_handle, &c, 1);
        if ((count <= 0) || (c == '\n'))// error or EOF or end of line
        {
            buf[idx++] = 0; // terminate string
            break;
        }
        else
        {
            buf [idx++] = c; // append character
        }
    }

    DBG(if (strlen(buf) != 0) cout << "FIFOInterface::Read - " << buf << endl;)
    return string(buf);
}

/**
 * FIFOInterface 
 * 
 * @param  {string} line : Line to be written to Serail out. Newline '\n' must be explicitly included
 */
void FIFOInterface::Write(string line)  {
    if (!isValid()) return;
    
    DBG(cout << "FIFOInterface::Write - " << line.c_str();)

    size_t count = write(_handle, line.c_str(), line.length());
    assert(count == line.length());
    
    
}
