
namespace Tools
{

template <typename T>
T Sign(const T &a);

template <typename T>
T Compare(const T &a, const T &b);

template <typename T>
bool CanStepUp(T current,  T step,  T limit);

template <typename T>
bool CanStepDown(T current,  T step,  T limit);

}