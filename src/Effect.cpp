#include "Effect.h"
#include "LED.h"
#include <math.h>

Effect::Effect(string name, string id) {
    _name = name;
    _id = id;
}

Effect::~Effect() {
    // nothing
    DBG(cout << "Effect::~Effect" << endl;)
}
/**
 * Effect 
 * Run effect processing thread
 */
void Effect::Run() {
    // thread will run Effect::render on object 'this' with argument _stopSignal.get_future()
    _stopSignal = false;
    _thread = thread(&Effect::render, this); 
    DBG(cout << "Effect::Run - Running thread" << endl;)
}

/**
 * Effect 
 * set the stop signal, ending the thread
 */
void Effect::Stop() {
    
    // set the stop signal, causing the future to trigger
    _stopSignal = true;

    // wait for thread to join
    if (_thread.joinable())
        _thread.join();
    DBG(cout << "Effect::Stop - Stopped thread" << endl;)
}    
/**
 * Effect 
 * Infinite loop that reads, processes, and writes audio samples
 * @param  {future<void>} stop : stop signal for thread
 */
void Effect::render() {
    // Loop until the stop signal is received

    // MCP3202 is the bottleneck with 50 kHz sample rate
    // target is to have the following look run at a 100 kHz
    // on a clean signal (do nothing), the plain C code of (https://github.com/ElectroSmash/pedal-pi/blob/master/clean.c) achieves around 186 kHz
    // This code achieves (measured without overclocking) > 175 kHz consistently, incl. serial writes
    // we are rate limiting it to 110 kHz to have a stable rate (some effects and the tuner will need this)
    chrono::time_point t0 = chrono::steady_clock::now();
    uint32_t samples = 0;
    const uint32_t max_samples = 100000;

    DBG(cout << "Effect::render - start loop"<< endl;)
    
    while (!_stopSignal)
    {
        // sleep simulation, variable is volatile to prevent compiler optimization
        volatile int32_t cnt = 0;
        while (cnt < _sleep) cnt++;

        readSample();
        mapIn();
        process(); // actual effect processing, polymorphic function call
        mapOut();
        writeSample();

        // measure sampling rate
        if (++samples == max_samples)
        {
            chrono::time_point t1 = chrono::steady_clock::now();
            uint32_t deltat = chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count();
            if (deltat) _samplingRate = (1000 * max_samples) / deltat;
            samples = 0;
            t0 = chrono::steady_clock::now();
            
            if (_samplingRate > 50)
            {
                // adjust sleep value to achieve target sampling rate
                int32_t sr = static_cast<int32_t> (_samplingRate);
                int32_t tsr = static_cast<int32_t> (TARGET_SAMPLING_RATE);
                int32_t adjust = ::round((sr - tsr)/1000.0f);
                DBG(cout << "sleep: " << _sleep << " adjust: " << adjust << " tsr: " << tsr << " sr: " << _samplingRate  << endl;)

                if (_sleep + adjust >= 0)
                    _sleep += adjust;
                else
                    _sleep = 0;
            }
            else
            {
                _sleep = 0;
            }
            
            
        }
    }
}

void Effect::UpdateState(const effectControl& control) {

    DBG(cout << "Effect::UpdateState"<< endl;)
    //light the effect when the footswitch is activated.   
    if (control.foot == ControlState::FOOT_BYPASS)     
        LED::Set(false); 
    else
        LED::Set(true); 
}
/**
 * Effect 
 * Read next input sample from ADC
 */
void Effect::readSample() {
    _inputSignal = ADDA::adc();
}

/**
 * Effect 
 * Write current output sample to DAC
 */
void Effect::writeSample() {
    ADDA::dac(_outputSignal);
}

/**
 * Effect 
 * Get effect-specific usage information
 * @return {effectHelp}  : Help struct
 */
effectHelp Effect::GetHelp() const {
    effectHelp h;
    h.left = getHelpLeft();
    h.right = getHelpRight();
    h.center = getHelpCenter();

    return h;
}

/**
 * Effect 
 * 
 * @return {int}  : last measured sampling rate
 */

int Effect::GetSamplingRate() const {
    return _samplingRate;
}

/**
 * Effect 
 * transform input signal to [-1, 1] \cap \QQ range
 */
void Effect::mapIn() {
    float sig = _inputSignal; // force typecast to ensure correct arithmetic 
    _signal = (sig - SIGNAL_SHIFT) / CENTERED_SIGNAL_MAX;
}
/**
 * Effect 
 * transform processed signal to the integer output range [0, 4095], inclusive clipping where required
 */
void Effect::mapOut() {
    
    // clip signal
    if (_signal > 1.0) _signal = 1.0;
    if (_signal < -1.0) _signal = -1.0;

    // transform back to integer range
    _outputSignal = _signal * CENTERED_SIGNAL_MAX + SIGNAL_SHIFT;

}

