#include "Effect.h"
#include <math.h>
#include <memory.h>

class BitCrusher: public Effect
{
private:
    atomic<int> _rateReduction = 1; 
    atomic<int> _resolutionReduction = 0;
    
    static const int RATE_EFFECT_STEP = 1; //factor
    static const int RATE_EFFECT_MIN = 1;
    static const int RATE_EFFECT_MAX = 50;

    static const int RES_EFFECT_STEP = 1;  // bits
    static const int RES_EFFECT_MIN = 0;
    static const int RES_EFFECT_MAX = 10;
    float RES_DEFAULT = ::pow(2.0, -12 + 1); // default resolution distributes 12 bits over the range [-1, 1] ;
    
    static const int BUFFER_SIZE = RATE_EFFECT_MAX;
    float _buffer[BUFFER_SIZE] = {0};
    int _count = 0;
    float _lastSignal = 0;

/***
     * Bit crusher has two parameters
     * 1) resolution reduction causes resampling of the input ot a resolution 12 - x bits
     * 2) sampling rate reduction causes the sampling rate to reduce by 20kHz times x
     * 
     * The effect maginitude is controlled via left (less reduction) and right (more reduction) buttons. 
     * The toggle switch selects A(resolution reduction) or B(sampling rate reduction)
     *  
     * */

public:
  BitCrusher()
  :Effect("BitCrusher", "bitcrusher") {
    

  };

protected:
    virtual void process();
    
    virtual string getHelpLeft() const
    {
        return string("-");
    };

    virtual string getHelpRight() const
    {
        return string("+");
    };

    virtual string getHelpCenter() const
    {
        return string("RES/RATE");
    };

    virtual void UpdateState(const effectControl& control )
    {
        // call base class state handler (footswitch LED control)
        Effect::UpdateState(control);

        if (control.toggle == ControlState::TOGGLE_A) // RESOLUTION REDUCTION
        {
            // decrease distortion on left button
            if (control.left == ControlState::BUTTON_PRESSED && Tools::CanStepDown(_resolutionReduction.load(), RES_EFFECT_STEP, RES_EFFECT_MIN))
            _resolutionReduction = _resolutionReduction - RES_EFFECT_STEP;
        
            // increase distortion on right button
            if (control.right == ControlState::BUTTON_PRESSED && Tools::CanStepUp(_resolutionReduction.load(), RES_EFFECT_STEP,  RES_EFFECT_MAX))
            _resolutionReduction = _resolutionReduction + RES_EFFECT_STEP;
        }
        else // RATE REDUCTION
        {
            // decrease distortion on left button
            if (control.left == ControlState::BUTTON_PRESSED && Tools::CanStepDown(_rateReduction.load(), RATE_EFFECT_STEP, RATE_EFFECT_MIN))
            _rateReduction = _rateReduction - RATE_EFFECT_STEP;
        
            // increase distortion on right button
            if (control.right == ControlState::BUTTON_PRESSED && Tools::CanStepUp(_rateReduction.load(), RES_EFFECT_STEP,  RATE_EFFECT_MAX))
            _rateReduction = _rateReduction + RATE_EFFECT_STEP;
        }
        
        
    };

    virtual string getState() const
    {
        //TODO: show both states in the correct order
        return string("1/") + to_string(_rateReduction) + string("|")  + to_string(_resolutionReduction) + string("b");
    };
};

