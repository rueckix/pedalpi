#include "Pedal.h"
#include <assert.h>
#include <iostream>
#include <sstream>
#include <chrono>

#include "alleffectheaders.h"


Pedal::Pedal(Interface& interface)
:_interface(interface) {
    initializeHardware();   
    initializeEffects();
    listEffects();
}

Pedal::~Pedal() {
    releaseHardware();
    // effect objects are deleted in  _effects descructor
    DBG(cout << "Pedal::~Pedal" << endl;)
}


/**
 * Pedal 
 * Adds all effects to the effect map. If a duplicate effect ID is detected, the code fires an assertion error
 */
void Pedal::initializeEffects() {
    // We are using a preprocessor to include and generate effects in alphabetical order
    #include "alleffects.h"

    // these lines are auto generated in Makefile
    //assert(_effects.Add(new Clean()));
    //assert(_effects.Add(new Distortion()));
}

void Pedal::switchEffect(string id /*= "clean"*/) {
    DBG(cout << "Pedal::switchEffect - Switching to " << id << endl;)
    Effect* e = _effects.Get(id);
    assert(e != nullptr);

    if (_current)
    {
        DBG(cout << "Pedal::switchEffect - Stopping Effect " << _current->GetName() << endl;)
        _current->Stop();
    }
    _current = e;    
    DBG(cout << "Pedal::switchEffect - Starting Effect " << _current->GetName() << endl;)
    _current->Run();

     // report effect and settings
    reportEffect();   
}

void Pedal::reportSamplingRate() {
    
    int sr = _current->GetSamplingRate();
    stringstream ss;
    ss << "%:" << sr/1000 << "kHz" << endl;
    //report sampling rate
    _interface.Write(ss.str());
    DBG(cout << "Pedal::reportSamplingRate - Sampling rate " << ss.str();)
}

void Pedal::reportEffect() {
    stringstream ss;
    ss << "!:" << _current->GetID();
    ss << endl;
    effectHelp h = _current->GetHelp();
    ss << "?:L:" << h.left << endl;
    ss << "?:R:" << h.right << endl;
    ss << "?:C:" << h.center << endl;

    _interface.Write(ss.str());
}

void Pedal::EventLoop() {
    
    // set default effect (clean)
    switchEffect();
    
    // Overview: https://www.libertyparkmusic.com/guitar-effect-pedals-types-and-sounds/
    // examples https://github.com/belangeo/pyo
    // another great resource http://www.musicdsp.org/en/latest/
    // imported library https://github.com/cycfi/Q
    
    // TODO: looper
    
    // TODO: octaver
    // TODO: try composing pedals with an EffectChain helper class
    // TODO: chorus
    // TODO: wah-wah
    // TODO: delay https://github.com/cycfi/Q/blob/master/example/delay.cpp
    // TODO: reverb
    // TODO: flanger
    // TODO: harmonizer
    // TODO: compressor
    // TODO: Fuzz
    
    while (true)
    {
        // sleep for a bit for debouncing and rate limiting 
        // OLED display is rather slow
        
        this_thread::sleep_for(std::chrono::milliseconds(250));

        // read input from serial console
        processCommand(); 

        // read hardware
        processControls(); 

        // report current sampling rate
        reportSamplingRate();

        // report current effect parameter
        reportStatus();


    }
}
/**
 * Pedal 
 * Set up BCM2835 pins, pwm modes, etc.
 */
void Pedal::initializeHardware() {
    #ifndef DUMMY
    // Start the BCM2835 Library to access GPIO.
    assert(bcm2835_init());
    
	// Start the SPI BUS.
	assert(bcm2835_spi_begin());

    //define PWM	
    bcm2835_gpio_fsel(18,BCM2835_GPIO_FSEL_ALT5 ); //PWM0 signal on GPIO18    
    bcm2835_gpio_fsel(13,BCM2835_GPIO_FSEL_ALT0 ); //PWM1 signal on GPIO13    
	bcm2835_pwm_set_clock(2); // Max clk frequency (19.2MHz/2 = 9.6MHz)
    bcm2835_pwm_set_mode(0,1 , 1); //channel 0, markspace mode, PWM enabled. 
	bcm2835_pwm_set_range(0,64);   //channel 0, 64 is max range (6bits): 9.6MHz/64=150KHz switching PWM freq.
    bcm2835_pwm_set_mode(1, 1, 1); //channel 1, markspace mode, PWM enabled.
	bcm2835_pwm_set_range(1,64);   //channel 0, 64 is max range (6bits): 9.6MHz/64=150KHz switching PWM freq.
	
	//define SPI bus configuration
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);      // The default
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);                   // The default
    bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_64); 	  // 4MHz clock with _64 
    bcm2835_spi_chipSelect(BCM2835_SPI_CS0);                      // The default
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);      // the default
	
    //Define GPIO pins configuration
    bcm2835_gpio_fsel(PUSHL, BCM2835_GPIO_FSEL_INPT); 			//PUSH1 button as input
	bcm2835_gpio_fsel(PUSHR, BCM2835_GPIO_FSEL_INPT); 			//PUSH2 button as input
	bcm2835_gpio_fsel(TOGGLE_SWITCH, BCM2835_GPIO_FSEL_INPT);	//TOGGLE_SWITCH as input
	bcm2835_gpio_fsel(FOOT_SWITCH, BCM2835_GPIO_FSEL_INPT); 	//FOOT_SWITCH as input
	bcm2835_gpio_fsel(LED_PIN, BCM2835_GPIO_FSEL_OUTP);				//LED as output
	
    bcm2835_gpio_set_pud(PUSHL, BCM2835_GPIO_PUD_UP);           //PUSH1 pull-up enabled   
	bcm2835_gpio_set_pud(PUSHR, BCM2835_GPIO_PUD_UP);           //PUSH2 pull-up enabled 
	bcm2835_gpio_set_pud(TOGGLE_SWITCH, BCM2835_GPIO_PUD_UP);   //TOGGLE_SWITCH pull-up enabled 
	bcm2835_gpio_set_pud(FOOT_SWITCH, BCM2835_GPIO_PUD_UP);     //FOOT_SWITCH pull-up enabled 
    #endif
}



/**
 * Pedal 
 * Release BCM2835 hardware
 */
void Pedal::releaseHardware() {
    #ifndef DUMMY
	//close all and exit
	bcm2835_spi_end();
    bcm2835_close();
    #endif
}

/**
 * Pedal 
 * Read Pedal-Pi controls (hardware buttons)
 * This function assumes external deboucing, it does not insert a delay after reading the pins
 */
void Pedal::processControls() {
    #ifndef DUMMY
	_control.left  = (bcm2835_gpio_lev(PUSHL) == ControlState::BUTTON_PRESSED) ? ControlState::BUTTON_PRESSED : !ControlState::BUTTON_PRESSED;
	_control.right = (bcm2835_gpio_lev(PUSHR) ==  ControlState::BUTTON_PRESSED) ? ControlState::BUTTON_PRESSED : !ControlState::BUTTON_PRESSED;
    _control.toggle = (bcm2835_gpio_lev(TOGGLE_SWITCH) == ControlState::TOGGLE_A) ? ControlState::TOGGLE_A : ControlState::TOGGLE_B;
	_control.foot = (bcm2835_gpio_lev(FOOT_SWITCH) == ControlState::FOOT_BYPASS) ? ControlState::FOOT_BYPASS: !ControlState::FOOT_BYPASS;
    
    DBG(if (_control.left == ControlState::BUTTON_PRESSED) cout << "Left button pressed" << endl;)
    DBG(if (_control.right == ControlState::BUTTON_PRESSED) cout << "Right button pressed" << endl;)
    DBG(if (_control.toggle == ControlState::TOGGLE_A) cout << "Toggle A" << endl;)
    DBG(if (_control.toggle == ControlState::TOGGLE_B) cout << "Toggle B" << endl;)
    DBG(if (_control.foot  == ControlState::FOOT_BYPASS) cout << "Foot bypass" << endl;)

	#else 
    // dummy code to make it compile without BCM 
    _control.foot = (bool) ControlState::FOOT_BYPASS; 
    _control.left = ! (bool) ControlState::BUTTON_PRESSED;
    _control.right = !(bool) ControlState::BUTTON_PRESSED;
    _control.toggle = ControlState::TOGGLE_A;
    #endif

    // pass button events to effect
    assert(_current);
    _current->UpdateState(_control);    
}


void Pedal::reportStatus() {
    assert(_current);
    _interface.Write(_current->GetState() + string("\n")); 
}

void Pedal::processCommand() {
    string s = _interface.Read(); 

    if (s.length() == 0) // nothing received
        return;

    // assumption
    // format X:YYYYY 
    // X is the command
    // YYYYY is the argument
    // get first letter = command

    if (s.length() <= 2) //invalid command length
        return; 

    string cmd = s.substr(0, 1);
    char c = cmd.c_str()[0];
    string arg = s.substr(2);

    DBG(cout << "Pedal::processCommand -- " << c << "(" << arg << ")" << endl;)

    if (c == 'S') // switch effect
        switchEffect(arg);    
}

void Pedal::listEffects() {
    // report all available effects
    effectNameList lst = _effects.GetList();
    for (auto iter = lst.begin(); iter != lst.end(); ++iter)
    {   
        stringstream ss;
        ss << "#:" << iter->first << ":" << iter->second  << endl;
        
        _interface.Write(ss.str());

        // rate limit to ensure that Arduino can process the serial backlog
        this_thread::sleep_for(std::chrono::milliseconds(250));

    }
}