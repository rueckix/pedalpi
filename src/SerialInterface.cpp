#include "SerialInterface.h"
#include <iostream>
#include <string.h>
#include "debug.h"
#include <assert.h>
#include <thread>
#include <chrono>

using namespace std;

SerialInterface::SerialInterface(string port) {
    // read, write,  avoid kill upon ctrl-c,  write synchronously
   _handle =  open(port.c_str(), O_RDWR |  O_NOCTTY | O_SYNC | O_NONBLOCK); 
   
    // wait for reset
    this_thread::sleep_for(std::chrono::seconds(3));

   if (_handle == -1)
   {
       DBG(cout << "Error opening serial port: " << errno << endl;)
       DBG(cout << "Error description is: " << strerror(errno) << endl;)
       throw; // throw exception
   }

    // Problem is that some hosts reset the serial line (DTR)
    // needs specific attributes

    speed_t brate = B9600;
    termios toptions;
    ::memset(&toptions, 0, sizeof(struct termios));

    // set i/o speed
    cfsetispeed(&toptions, brate);
    cfsetospeed(&toptions, brate);

    // 8N1
    toptions.c_cflag &= ~PARENB;
    toptions.c_cflag &= ~CSTOPB;
    toptions.c_cflag &= ~CSIZE;
    toptions.c_cflag |= CS8;
    
    // no flow control
    toptions.c_cflag &= ~CRTSCTS;
    toptions.c_cflag     |=  CREAD | CLOCAL;     // turn on READ & ignore ctrl lines
    toptions.c_iflag |= IGNPAR | IGNCR;
    toptions.c_iflag &= ~(IXON | IXOFF | IXANY);
    toptions.c_lflag |= ICANON;
    

    tcflush( _handle, TCIFLUSH );
    tcsetattr(_handle, TCSANOW, &toptions);
    

    if( tcsetattr(_handle, TCSAFLUSH, &toptions) < 0) {
        DBG(cout << "Error setting terminal attributes: " << errno << endl;)
        DBG(cout << "Error description is: " << strerror(errno) << endl;)
        throw; // throw exception
    }
}

SerialInterface::~SerialInterface() {
    if (isValid()) close(_handle);
}
/**
 * SerialInterface 
 * 
 * @return {string}  : String (max 80 characters) read from serial interface. Strings are read until '\n' or EOF
 */
string SerialInterface::Read() {
    if (!isValid()) return string("");
    
    const int size = 80;
    char buf[size];
    int idx = 0;
    for (int i = 0; i < size; i++)
    {
        char c;
        int count = read(_handle, &c, 1);
        if ((count <= 0) || (c == '\n'))// error or EOF or end of line
        {
            buf[idx++] = 0; // terminate string
            break;
        }
        else
        {
            buf [idx++] = c; // append character
        }
    }

    DBG(if (strlen(buf) != 0) cout << "SerialInterface::Read - " << buf << endl;)
    return string(buf);
}
/**
 * SerialInterface 
 * 
 * @param  {string} line : Line to be written to Serail out. Newline '\n' must be explicitly included
 */
void SerialInterface::Write(string line)  {
    if (!isValid()) return;

    size_t count = write(_handle, line.c_str(), line.length());
    assert(count == line.length());
    
    

    DBG(cout << "SerialInterface::Write - " << line.c_str();)
}
