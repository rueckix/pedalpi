#include "fxBitCrusher.h"

void BitCrusher::process()
{

    // 0. Update the history buffer
    for (int i = 1; i < BUFFER_SIZE; i++)
    {
        _buffer[i-1] = _buffer[i];
    }
    _buffer[BUFFER_SIZE-1] = _signal;

    // 1. low pass filtering and decimation (via sample and hold)
    if (++_count >= _rateReduction) // done holding, reset counter
    {
        _count = 0;
        float out = 0.0f;
        for (int i = 0; i < _rateReduction; i++)
        {
            out += _buffer[BUFFER_SIZE -1 - i];
        }
        
        _signal = out / (float) _rateReduction;
        _lastSignal = _signal;
    }
    else
    {
        _signal = _lastSignal;
    }
    

    // 2. sampling resolution reduction effect
    float q = RES_DEFAULT * ::pow(2.0, _resolutionReduction);
    
    // round to nearest quantisation step
    _signal = ::round(_signal / q) * q;
}
