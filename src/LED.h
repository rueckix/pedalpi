#include "bcm.h"

class LED
{
public:
    static void Set(bool on)
    {
        #ifndef DUMMY
        bcm2835_gpio_write(LED_PIN,on); //light the effect when the footswitch is activated.
        DBG(cout << "Switching LED: " << on << endl;)
        #else
        on = on; // dummy code to avoid unused paramter warning
        #endif
    };
};
