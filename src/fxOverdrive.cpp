#include "fxOverdrive.h"

void Overdrive::process()
{
    // symmetrical soft clipping
    if (fabs(_signal) < 0.33)
            _signal = 2 * _signal;
    else if (fabs(_signal) < 0.66)
            _signal = _amount  * Tools::Sign(_signal) * (3-(2- 3* fabs(_signal))* (2- 3*  fabs(_signal)))/3.0;
    else
            _signal = Tools::Sign(_signal) * _amount;

}