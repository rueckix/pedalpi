
#pragma once

// enable or disable tracing
// #define LOG

#ifdef LOG
#define DBG(X) X
#else
#define DBG(X) {}
#endif
