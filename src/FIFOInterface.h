#include "Interface.h"


class FIFOInterface: public Interface
{
private:
    int _handle = -1;

public:
    FIFOInterface(string port);
    ~FIFOInterface();

    virtual string Read();

    virtual void Write(string line);

private:
    bool isValid() {return (_handle != -1);};
};


