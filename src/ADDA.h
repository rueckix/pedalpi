#include <stdint.h>
#include "bcm.h"

class ADDA
{
public:
    static uint32_t adc()
    {
        #ifndef DUMMY
        //read 12 bits ADC
        char mosi[10] = { 0x01, 0x00, 0x00 }; //12 bit ADC read 0x08 ch0, - 0c for ch1 
	    char miso[10] = { 0 };
        bcm2835_spi_transfernb(mosi, miso, 3);
        return miso[2] + ((miso[1] & 0x0F) << 8); 
        #else
        return 0;
        #endif
    };

    static void dac(uint32_t signal)
    {
        #ifndef DUMMY
        //generate output PWM signal 6 bits
        bcm2835_pwm_set_data(1,signal & 0x3F);
        bcm2835_pwm_set_data(0,signal >> 6);
        #else
        signal = signal; // dummy code to avoid unused parameter warning
        #endif
    };
};
