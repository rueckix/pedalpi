#include "fxDistortion.h"
#include <limits>
#include <sstream>

Distortion::Distortion()
: Effect("Distortion", "distortion") {
    
}


/**
 * Distortion 
 * Clip signal to [-_amount, +_amount]
 */
void Distortion::process() {
    if (_signal > _amount) _signal= _amount;
	if (_signal < -_amount) _signal= -_amount;
}

string Distortion::getHelpLeft() const {
    return string("-");
}

string Distortion::getHelpRight() const {
    return string("+");
}

string Distortion::getHelpCenter() const {
    return string("");
}
/**
 * Distortion 
 * Update the _distortion value in range [0,1]
 * @param  {effectControl} control : 
 */
void Distortion::UpdateState(const effectControl& control )
{
    // call base class state handler (footswitch LED control)
    Effect::UpdateState(control);

    // Note: the below uses a safe float to float comparison that takes care of any internal precision issues
    // Note: a small distortion value means less distortion since we are clipping to a range based on that distortion value
    // decrease distortion on left button
    if (control.left == ControlState::BUTTON_PRESSED && Tools::CanStepUp(_amount.load(), EFFECT_STEP, EFFECT_MAX))
        _amount = _amount + EFFECT_STEP;
    
    // increase distortion on right button
    if (control.right == ControlState::BUTTON_PRESSED && Tools::CanStepDown(_amount.load(), EFFECT_STEP, EFFECT_MIN))
        _amount = _amount - EFFECT_STEP;
}

/**
 * Distortion 
 * 
 * @return {string}  : string representation of the distortion value
 */
string Distortion::getState() const {
    // map to simple integer range. Small value of _distortion means high distortion
    int val = 100* (EFFECT_MAX - _amount);
    //std::ostringstream out;
    //out.precision(2); // have only one decimal
    //out << std::fixed << _distortion;
    //return out.str();
    return to_string(val);
}