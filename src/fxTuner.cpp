#include "fxTuner.h"

void Tuner::process()
{
    // Bandpass filter
    float s = _lp(_signal); // low
    s -= _lp2(s); // high

    // Mute signal
    _signal = 0;

    // Envelope
    auto e = _env(std::abs(s));

    if (e > _threshold)
    {
        // Compressor + makeup-gain + hard clip
        auto gain = float(_comp(e)) * _makeup_gain;
        s = _clip(s * gain);
        _threshold = _release_threshold;
    }
    else
    {
        s = 0.0f;
        _threshold = _onset_threshold;
    }

    
    bool ready = _pd(s);
    if (ready)
        _prediction = _pd.predict_frequency();
}

string Tuner::getState() const
{
    ostringstream ss;

    float freq = _prediction;
    if (freq < (float) MIN_FREQ)
        return string("--"); // nothing detected


    // find octave 
    int octave = 0;
    while (Tools::Compare(freq, (float) q::notes::B[octave]) == 1) // while "closest" is higher than B[octave]
    {
        octave++;
        if (octave > 7)
        { // frequency not plausible
            return string("infty");
        }
    }

    // now: freq >= B[octave] && freq <= C[octave]
    
    // determine closest note
    float closest = (float) q::notes::C[octave]; 
    int note_index = 0;
    int cur_idx = 0;
    // determine error. Negative if freq sharp. Positive if freq is flat.
    float error = closest - freq;

    for (q::frequency f = q::notes::C[octave]; f <= q::notes::B[octave]; f = q::next_frequency(f), cur_idx++)
    {
        if (Tools::Compare(fabs((float) f - freq), fabs(error)) == -1) // new error is smaller
        {
            // update best fit
            closest = (float) f;
            error = closest - freq;
            note_index = cur_idx;
        }
    }

    ss << _note_names[note_index];
    ss << octave;

    // approximate maxium error via comparison to next frequency
    float next = (float) q::next_frequency(closest);
    float max_error = (next - closest) / 2.0f;

    // map error for display
    int num = round(error / max_error * 3.0);
    // extract sign
    bool sharp = (Tools::Sign(num) < 1);
    // remove sign
    num = abs(num);

    if (num > 0)
        ss << " ";
    else
        ss << "===";

    for (int i = 0; i < num; i++)
    {
        if (sharp)
            ss << "#";
        else
            ss << "b";
    }

    return ss.str();
}