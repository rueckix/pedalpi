#include <map>


#include "Effect.h"

using namespace std;

typedef map<string, Effect*> effectMap;
typedef map<string, string> effectNameList;

class EffectMap
{
    private:
        effectMap _effects;
    
    public:
        ~EffectMap()
        {
            // delete all effect objects
            for (auto itr = _effects.begin(); itr != _effects.end(); ++itr) {
                DBG(cout << "EffectMap::~EffectMap - deleting " << itr->first << endl;)
                delete itr->second;
            }

            

            DBG(cout << "EffectMap::~EffectMap" << endl;)
        }
        /**
         * 
         * @param  {Effect*} effect : Effect to insert
         * @return {bool}           : returns true upon success, false if (effect==NULL or duplicate effect ID is detected)
         */
        bool Add(Effect* effect)
        {
            if (!effect)
                return false;

            auto ret = _effects.insert({effect->GetID(), effect});
            return ret.second; 
        }

        /**
         * 
         * @param  {string} id : effect identifier to look up
         * @return {Effect*}   : effect reference or nullptr
         */
        Effect* Get(string id)
        {
            try
            {
                Effect* e = _effects.at(id);
                return e;
            } catch(...)
            {
                return nullptr;
            }
        }

        /**
         * 
         * @return {effectNameList}  : The list of available effects. Pairs {(id, name)}.
         */
        effectNameList GetList()
        {
            effectNameList list;
            

            for (auto itr = _effects.begin(); itr != _effects.end(); ++itr) {
                string id = itr->first;
                string name = itr->second->GetName();
                list.insert({id, name});
            }

            return list;
        }
};
