#include "Effect.h"
#include <math.h>
#include <memory.h>
#include <sstream>

#include <q/pitch/pitch_detector.hpp>
#include <q/support/literals.hpp>
#include <q/support/notes.hpp>
#include <q/fx/envelope.hpp>
#include <q/fx/lowpass.hpp>
#include <q/fx/waveshaper.hpp>
#include <q/fx/biquad.hpp>
#include <q/fx/dynamic.hpp>


namespace q = cycfi::q;
using namespace q::literals;

class Tuner: public Effect
{
    

private:

    const uint32_t SPS = TARGET_SAMPLING_RATE; // empirically determined
    // sps is important because it is used to calculate the frequency prediction
    // maybe we can set it based on measured performance
    const q::frequency          MAX_FREQ =  q::notes::E[6];
    const q::frequency          MIN_FREQ = q::notes::E[1];
    q::pitch_detector           _pd{MIN_FREQ ,MAX_FREQ, SPS, -30_dB };

    q::one_pole_lowpass        _lp{MAX_FREQ, SPS };
    q::one_pole_lowpass        _lp2{MIN_FREQ , SPS};
    q::peak_envelope_follower  _env{ 30_ms, SPS };

    const char* _note_names[12] = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};
	atomic<float> _prediction = 0; // predicted frequency

	const float                    _slope = 1.0f/4.0;
        const float                _makeup_gain = 4;
        q::compressor              _comp{ -18_dB, _slope };
        q::clip                    _clip;
        float                      _onset_threshold = float(-28_dB);
        float                      _release_threshold = float(-60_dB);
        float                      _threshold = _onset_threshold;



public:
  Tuner()
  :Effect("Tuner", "tuner") {
    

  };

protected:
    virtual void process();
 

    virtual string getHelpLeft() const
    {
        return string("");
    };

    virtual string getHelpRight() const
    {
        return string("");
    };

    virtual string getHelpCenter() const
    {
        return string("");
    };


    virtual string getState() const;
};

