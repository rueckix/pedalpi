#include "fxDelay.h"
#include <string>

void Delay::process()
{
     // TODO: handle delay and feedback processors
}

void Delay::UpdateState(const effectControl& control )
{
    // call base class state handler (footswitch LED control)
    Effect::UpdateState(control);

//TODO: handle both controls in the correct order (LEFT delay, RIGHT feedback)
// TODO: create new delay and feedback objects when the settings change. Make sure we delete the old ones also ensure that we are handling this in a thread safe manner


    

    if (control.toggle == ControlState::TOGGLE_A) // FEEDBACK 
    {
        // decrease distortion on left button
        if (control.left == ControlState::BUTTON_PRESSED && Tools::CanStepDown(_feedback.load(), FEEDBACK_EFFECT_STEP, FEEDBACK_EFFECT_MIN))
        _feedback = _feedback - FEEDBACK_EFFECT_STEP;
    
        // increase distortion on right button
        if (control.right == ControlState::BUTTON_PRESSED && Tools::CanStepUp(_feedback.load(), FEEDBACK_EFFECT_STEP,  FEEDBACK_EFFECT_MAX))
        _feedback = _feedback + FEEDBACK_EFFECT_STEP;
    }
    else // Duration
    {
        // decrease distortion on left button
        if (control.left == ControlState::BUTTON_PRESSED && Tools::CanStepDown(_duration.load(), DELAY_EFFECT_STEP, DELAY_EFFECT_MIN))
        _duration = _duration - DELAY_EFFECT_STEP;
    
        // increase distortion on right button
        if (control.right == ControlState::BUTTON_PRESSED && Tools::CanStepUp(_duration.load(), DELAY_EFFECT_STEP,  DELAY_EFFECT_MAX))
        _duration = _duration + DELAY_EFFECT_STEP;
    }
}


string Delay::getState() const
{
    ostringstream ss;
//TODO: show both states in the correct order
    return ss.str();
    

}