#include "Effect.h"
#include <math.h>

class Overdrive: public Effect
{
private:
    atomic<int> _amount = 1; 
    
    static const int EFFECT_STEP = 1; 
    static const int EFFECT_MIN = 1;
    static const int EFFECT_MAX = 20;

    
/***
     * Overdrive has one parameter
     * 
     * It uses symmetrical soft clipping as per 
     * https://www.dafx12.york.ac.uk/papers/dafx12_submission_45.pdf
     * 
     * The effect maginitude is controlled via left (less distortion) and right (more distortion) buttons. 
     * The toggle switch has no effect
     *  
     * */

public:
  Overdrive()
  :Effect("Overdrive", "overdrive") {
    

  };

protected:
    virtual void process();

    virtual string getHelpLeft() const
    {
        return string("-");
    };

    virtual string getHelpRight() const
    {
        return string("+");
    };

    virtual string getHelpCenter() const
    {
        return string("");
    };

    virtual void UpdateState(const effectControl& control )
    {
        // call base class state handler (footswitch LED control)
        Effect::UpdateState(control);

        // decrease distortion on left button
        if (control.left == ControlState::BUTTON_PRESSED && Tools::CanStepDown(_amount.load(), EFFECT_STEP, EFFECT_MIN))
        _amount = _amount - EFFECT_STEP;
    
        // increase distortion on right button
        if (control.right == ControlState::BUTTON_PRESSED && Tools::CanStepUp(_amount.load(), EFFECT_STEP, EFFECT_MAX))
        _amount = _amount + EFFECT_STEP;
        
        
    };

    virtual string getState() const
    {
        return to_string(_amount);
    };
};

