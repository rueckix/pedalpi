#pragma once

#include <string>

using namespace std;

class Interface
{
public:
    virtual string Read() = 0;
    virtual void Write(string line) = 0;
};    
