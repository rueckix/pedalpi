#include "Effect.h"
#include <math.h>
#include <memory.h>
#include <sstream>

// HACK to remove delay macro that conflics with delay.hpp when using gcc on Alpine/Raspberry 
#undef delay 
#include <q/support/literals.hpp>
#include <q/fx/delay.hpp>

namespace q = cycfi::q;
using namespace q::literals;

class Delay: public Effect
{
    

private:

    const uint32_t SPS = TARGET_SAMPLING_RATE; 
    // sps is important because it is used to calculate the frequency prediction
    // maybe we can set it based on measured performance
    
    atomic<float> _duration = (float)0_ms; // delay duration
    atomic<float> _feedback = 1; // feedback factor

// TODO: look into makeing the feedback and delay objects atomic

    const float DELAY_EFFECT_STEP =  (float)100_ms; //factor
    const float DELAY_EFFECT_MIN = (float)0_ms;
    const float DELAY_EFFECT_MAX =  (float)2_s;
	
    const float FEEDBACK_EFFECT_STEP =  0.1; 
    const float FEEDBACK_EFFECT_MIN = 0.1;
    const float FEEDBACK_EFFECT_MAX =  1.0;
	


public:
  Delay()
  :Effect("Delay", "delay") {
    

  };

protected:
    virtual void process();
 

    virtual string getHelpLeft() const
    {
        return string("-");
    };

    virtual string getHelpRight() const
    {
        return string("+");
    };

    virtual string getHelpCenter() const
    {
        return string("d/feed");
    };

    virtual void UpdateState(const effectControl& control );
    virtual string getState() const;
};

