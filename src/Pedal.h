#include "Effect.h"
#include "Interface.h"

#include <map>
#include "EffectMap.h"
#include "bcm.h"

using namespace std;


class Pedal
{
private:
    Effect* _current = nullptr;
    EffectMap _effects;
    effectControl _control;
    Interface& _interface;

public:
    Pedal(Interface &interface);
    ~Pedal();

    void EventLoop();
    

private:
    void initializeHardware();
    void initializeEffects();
    void releaseHardware();
    void processControls();
    void processCommand();
    void reportStatus();
    void reportSamplingRate();
    void reportEffect();
    void switchEffect(string id = string("clean"));
    void listEffects();
};
