#pragma once

// dummy compilation will exclude all BCM related stuff, so that we can develop logic outside of the raspberry
#ifndef __arm__
#define DUMMY 
#endif

#ifndef DUMMY
#include <bcm2835.h>
#define PUSHL 			RPI_GPIO_P1_08  	//GPIO14
#define PUSHR 			RPI_V2_GPIO_P1_38  	//GPIO20 
#define TOGGLE_SWITCH 	RPI_V2_GPIO_P1_32 	//GPIO12
#define FOOT_SWITCH 	RPI_GPIO_P1_10 		//GPIO15
#define LED_PIN  			RPI_V2_GPIO_P1_36 	//GPIO16
#endif


// mapping of hardware states to logical states
class ControlState
{
        public:
        static const int BUTTON_PRESSED = 0;  // push button is pressed
        static const bool TOGGLE_A = 0;        // toggle switch is in A position
        static const bool TOGGLE_B = 1;       // toggle switch is in B position
        static const bool FOOT_BYPASS = 1;     // foot switch is in BYPASS state
};



