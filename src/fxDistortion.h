#include "Effect.h"

class Distortion: public Effect
{
private:

    const float EFFECT_STEP = 0.01;
    const float EFFECT_MIN = 0.01;
    const float EFFECT_MAX = 0.31;
    atomic<float> _amount = EFFECT_MAX; 

public:
    Distortion();
  
protected:
    virtual void process();
    virtual string getHelpLeft() const;
    virtual string getHelpRight() const;
    virtual string getHelpCenter() const;
    virtual void UpdateState(const effectControl& control );
    virtual string getState() const;
};