#include "fxClean.h"


Clean::Clean()
: Effect("Clean", "clean") {
    
}

void Clean::process() {
    // nothing
}

string Clean::getHelpLeft() const {
    return string("");
}

string Clean::getHelpRight() const {
    return string("");
}

string Clean::getHelpCenter() const {
    return string("");
}

string Clean::getState() const {
    return string("");
}
